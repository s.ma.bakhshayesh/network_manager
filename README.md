# network_manager

A Network Manager package based on dio for easy REST API management

## Getting Started


A Network Manager package based on dio for easy REST API management
there is just some functionality written for using post and get api easily

## Usage1

it's suggested that initialize network manager at some entry point in app
you can set BaseUrl, TimeOut, and some pre-written function that will be called
on every request : onStartDefault, onEndDefault, onSuccessDefault, onFailedDefault.

```dart
    initializeNetworkManager({String token, @required String baseURL}) {
      NetworkOption.initialize(
          baseUrl: baseURL,
          timeout: 30000,
          token: token,
          headers: {
            'Content-Type': 'application/json',
            "Authorization": "Bearer $token",
          },
          onStartDefault: (req) {
            print("Start");
          },
          onEndDefault: (req) {
            print("End");
          },
          onSuccessDefault: (res) {
            print("Success");
          },
          onFailedDefault: (NetworkResponse res) {
            print("Failed");
          },
          errorMsgExtractor: (res) {
            return res["Message"] ?? "Unknown Error";
          },
          successMsgExtractor: (res) {
            return res["Message"] ?? "Done";
          });
    }
```

every Network Request returns a Future<NetworkResponse> which handles variant situations with following properties:

```dart
class NetworkResponse {
  final bool responseStatus;
  final int responseCode;
  final String responseTitle;
  Function retryFunction;
  dynamic responseBody;
  dynamic responseDetails;
  String extractedMessage;

  NetworkResponse(
      {this.responseStatus,
        this.responseCode,
        this.responseTitle,
        this.responseBody,
        this.retryFunction,
        this.responseDetails,
        this.extractedMessage});

  @override
  String toString() {
    return "$responseCode $responseTitle : $extractedMessage";
  }
}

```

and responseBody is JsonDecode of base dio response.data

## Example
every where you want to make a request you can do as follows:

```dart
final Map<String,dynamic> req ={
      "Username": "test",
      "Password": "test",
    };

    NetworkManager networkManager = NetworkManager.instance(
      context: context,
      onSuccess: (context,response)=>print("OnSuccessCalled"),
      req: req,
      api: Apis.login
    );
    networkManager.requestPost();
```


## Usage2

it's suggested that initialize network manager at some entry point in app
you can set BaseUrl, TimeOut, and some pre-written function that will be called
on every request : onStartDefault, onEndDefault, onSuccessDefault, onFailedDefault.

```dart
      initializeNetworkManager() {
        /// initialize network manager
        NetworkManagerMetaData.initialize(
            baseUrl: "http://example.com",
            timeout: 30000,
            token: token,
            headers: {
              'Content-Type': "application/json",
            },
            onStartDefault: () {
              print("Start");
            },
            onEndDefault: () {
              print("End");
            },
            onSuccessDefault: () {
              print("Success");
            },
            onFailedDefault: () {
              print("Failed");
            },
            successRules: (result) {
              return (result["Status"] ?? -1) > 0;
            },
            tokenExpireRules: (resultJson) {
              return (resultJson["Status"] == -999);
            },
            onTokenExpire: (retryFunction) {
              print("Failed");
              // can call retry function
            },
            useFancyDialog: true,
            errorMsgExtractor: (result) {
              return result["Message"] ?? "Unknown Error";
            });
      }
```

## Example
every where you want to make a request you can do as follows:

```dart
final Map<String,dynamic> req ={
      "Username": "test",
      "Password": "test",
    };

    NetworkManager networkManager = NetworkManager.instance(
      context: context,
      onSuccess: (context,response)=>print("OnSuccessCalled"),
      req: req,
      api: Apis.login
    );
    networkManager.requestPost();
```



