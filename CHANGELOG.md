## [0.0.1] - 2020-07-25
    initial release

## [0.0.2] - 2020-07-25
    moved successRules to initialize
    added function types

## [0.0.3] - 2020-07-28
    dialog implements using handy_ui_kit
    (success/error)MsgExtractors added to initialize

## [0.0.4] - 2020-07-28
    initialize method null values handled

## [0.0.5] - 2020-07-28
    retry button

## [0.0.6] - 2020-07-28
    jsonDecodeResponse

## [0.0.7] - 2020-07-28
    jsonDecodeResponse fixed

## [0.0.8] - 2020-07-28
    no more singleton dio
    added metaData

## [0.0.9] - 2020-07-28
    decode bugs fixed
    readme update

## [0.0.10] - 2020-08-04
    cancel token added

## [0.0.11] - 2020-08-05
    token expire added

## [0.0.12] - 2020-08-05
    on tokenExpire gets context
    on start on end defaults fixed

## [0.0.13] - 2020-08-15
    dynamic req

## [0.0.14] - 2020-08-15
    dynamic req fixed

## [0.0.15] - 2020-08-16
    headers for instance

## [0.0.16] - 2020-08-16
    headers for instance fixed

## [0.0.17] - 2020-08-16
    returned error is Error object instead of e.error

## [0.0.18] - 2020-08-26
   default timeout to 15sec

## [0.0.19] - 2020-09-02
   default show failed dialog =>true

## [0.0.20] - 2020-09-07
   check for null response.data

## [0.0.21] - 2020-09-13
   jsonPrintLog

## [0.0.22] - 2020-10-03
   jsonPrintLog

## [0.0.23] - 2020-10-03
   connection error

## [0.0.24] - 2020-10-03
   connection error

## [0.0.25] - 2020-10-03
   connection failed with status code

## [0.0.26] - 2020-10-06
   all dio error type is handled

## [0.0.27] - 2020-10-10
   on failed Error fixed

## [0.0.28] - 2020-10-20
   async post and async network manager

## [0.0.29] - 2020-10-20
   exception throwing

## [0.0.30] - 2020-10-27
   define showDialog in initialize

## [0.0.31] - 2020-10-27
   define showDialog in initialize fixed

## [0.0.32] - 2020-10-27
   add useFancy for dialog

## [0.0.33] - 2020-10-27
   handy ui kit =>0.0.25

## [0.0.34] - 2020-10-27
   handy ui kit =>0.0.26
   fixed cancel button

## [0.0.35] - 2020-10-28
   properties added for instance (redirect, timeout,validStatus)

## [0.0.36] - 2020-10-28
   null success/failed dialog handled

## [0.0.37] - 2020-10-28
   onFailed onSuccess return class

## [0.0.38] - 2020-10-28
   onFailed onSuccess return class 2


## [0.0.39] - 2020-10-28
   onFailed onSuccess return class 3 fixed

## [0.0.40] - 2020-10-28
   onFailed onSuccess return class 4 fixed

## [0.0.41] - 2020-10-31
   success rules failure fixed

## [0.0.43] - 2020-11-14
   Socket Manager Added

## [0.0.44] - 2020-11-15
   Socket Manager fixed

## [0.0.45] - 2020-11-16
   global Cancel token added

## [0.0.46] - 2020-11-22
   call checkers fixed

## [0.0.47] - 2020-11-22
   onReceive don't stack

## [0.0.48] - 2020-11-29
   endRules for socket and start on sending

## [0.0.49] - 2020-11-29
   handle error problem

## [0.0.50] - 2020-11-29
   onDone

## [0.0.51] - 2020-11-29
   onDone fix


## [0.0.52] - 2020-11-29
   network error msg

## [0.0.53] - 2020-11-30
   exception throwing
   error msg improvement

## [0.0.54] - 2020-11-30
   exception throwing fix

## [0.0.55] - 2020-11-02
   sending timeout error

## [0.0.56] - 2020-11-02
   timeout error msg

## [0.0.57] - 2020-11-02
   retry function added to response and onTokenExpire

## [0.0.58] - 2020-11-02
   retry function added to response and onTokenExpire fix

## [0.0.59] - 2020-11-02
   automatic token refreshment from meta

## [0.0.60] - 2020-11-02
   automatic token refreshment from meta fix


## [0.0.61] - 2021-01-10
   error type response Message extract


## [0.0.62] - 2021-01-10
   error type response Message extract


## [0.0.63] - 2021-01-10
   error type response Message extract fix

## [0.0.64] - 2021-01-10
   error type response Message extract fix 1

## [0.0.65] - 2021-01-10
   error type response Message extract fix 2

## [0.0.66] - 2021-01-10
   error type response Message extract fix 3

## [0.0.67] - 2021-01-10
   error type response Message extract fix 4

## [0.0.68] - 2021-01-11
   cleaning structure

## [0.0.69] - 2021-01-11
   cleaning structure 1

## [0.0.70] - 2021-01-11
   error type response Message extract fix 5

## [0.0.71] - 2021-02-22
   remove cancel on Network error option

## [0.0.72] - 2021-02-22
   remove cancel on Network error option fix

## [0.0.73] - 2021-02-23
   remove cancel on Network error option fix 2

## [0.0.74] - 2021-02-24
   new manager added

## [0.0.75] - 2021-03-14
   new manager added fix

## [0.0.76] - 2021-03-15
   handle error no defaults

## [0.0.77] - 2021-03-15
   handle error no defaults fix

## [0.0.78] - 2021-03-15
   handle error no defaults fix 2

## [0.0.79] - 2021-03-16
   retry given added

## [0.0.80] - 2021-03-16
   retry given added fix

## [0.0.81] - 2021-04-07
   dio update

## [0.0.82] - 2021-04-07
   dio update fix

## [0.0.83] - 2021-04-11
   put get delete download added

## [0.0.84] - 2021-04-11
   uikit update

## [0.0.85] - 2021-05-12
   NULL SAFETY

## [0.0.86] - 2021-05-26
   SDK fix

## [0.0.87] - 2021-05-27
   token Expire fix

## [0.0.88] - 2021-05-27
   token Expire fix 2

## [0.0.89] - 2021-05-27
   token Expire improve

## [0.0.90] - 2021-05-27
   handy ui kit update

## [0.0.91] - 2021-05-27
   fix

## [0.0.92] - 2021-05-27
   fix 2
   fix

## [0.0.93] - 2021-06-09
   extra success rules added

## [0.0.94] - 2021-06-20
   handle bools added to nr

## [0.0.95] - 2021-06-21
   handle bools added to nr fix

## [0.0.96] - 2021-06-27
    old nm fix

## [0.0.97] - 2021-06-27
    old nm fix 2

## [0.0.98] - 2021-06-27
    old nm fix 3

## [0.0.99] - 2022-03-13
    web token fix

## [0.1.0] - 2022-03-13
    updating dio
    warning fixes

## [0.1.1] - 2022-05-28
   old manager timeout fix

## [0.1.2] - 2022-05-28
   timeout fix
   followRedirects

## [0.1.3] - 2022-09-06
   remove handy ui kit

## [0.1.4] - 2022-09-07
   supports notJson response

## [0.1.5] - 2022-11-20
   dependency update


## [0.1.6] - 2022-11-20
  added request to onStart and onEnd
   dependency update

## [0.1.7] - 2022-04-04
  compression option

## [0.1.8] - 2022-04-05
vec icon update

## [0.1.8] - 2022-04-05
comp fix


## [0.1.9] - 2022-04-05
comp fix2


## [0.1.10] - 2022-04-05
comp fix3

## [0.1.11] - 2022-04-05
comp changes

## [0.1.12] - 2022-04-05
comp fix web

## [0.1.13] - 2022-04-05
comp fix web 2

## [0.1.14] - 2022-04-05
remove date time picker

## [0.1.15] - 2023-07-24
remove date time picker

## [0.1.16] - 2023-07-25
added request to functions

## [0.1.17] - 2023-07-25
add force to retry

## [0.1.18] - 2023-07-25
add force to retry fix


## [0.1.20] - 2023-07-25
add response to retry

## [0.1.20] - 2023-07-25
change http adaptor

## [0.1.22] - 2024-05-02
dio desktop problem fix




