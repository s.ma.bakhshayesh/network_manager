import 'package:flutter/material.dart';

enum DialogType { success, error, warning }

class MyDialog extends StatefulWidget {
  final String title;
  final DialogType type;
  final String body;
  final String? confirmText;
  final bool needDescription;
  final String descriptionLabel;
  final Widget? extra;
  final double borderRadius;
  final EdgeInsets contentPadding;
  final EdgeInsets padding;
  final bool needPassword;
  final bool hasCancel;
  final String defaultButtonText;

  const MyDialog(
      {
        this.title = "warning",
        this.type = DialogType.warning,
        this.body = "",
        this.confirmText,
        this.needDescription = false,
        this.extra,
        this.defaultButtonText = "OK",
        this.hasCancel = true,
        this.needPassword = false,
        this.borderRadius = 12,
        this.contentPadding = const EdgeInsets.all(4),
        this.padding = const EdgeInsets.all(8),
        this.descriptionLabel="Description"});

  @override
  State<StatefulWidget> createState() {
    return _MyDialogState();
  }
}

class _MyDialogState extends State<MyDialog> {
  TextEditingController msgController = TextEditingController();
  TextEditingController passController = TextEditingController();
  bool _showPassword=false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(widget.borderRadius),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      insetPadding: widget.padding,
      content: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return _buildMainBody(context);
  }

  _buildHeader() {
    if (widget.type == DialogType.success) {
      return Image.asset('assets/images/successHeader.png', package: 'handy_ui_kit');
    } else if (widget.type == DialogType.error) {
      return Image.asset('assets/images/errorHeader.png', package: 'handy_ui_kit');
    } else {
      return Image.asset('assets/images/alertHeader.png', package: 'handy_ui_kit');
    }
  }

  _buildMainBody(context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
        constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height *0.5),
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(widget.borderRadius),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            _buildHeader(),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      padding: widget.contentPadding,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[Text(widget.title, style: TextStyle(fontWeight: FontWeight.bold)), Text(widget.body)],
                      ),
                    ),
                    widget.extra != null ? widget.extra! : Container(),
                    widget.needDescription ? _buildMsgField() : Container(),
                    widget.needPassword ? _buildPasswordField() : Container(),
                  ],
                ),
              ),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: widget.confirmText == null
                  ? <Widget>[
                TextButton(
                  child: Text(
                    widget.defaultButtonText,
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  onPressed: () => Navigator.pop(context),
                )
              ]
                  : <Widget>[
                widget.hasCancel
                    ? TextButton(
                  child: Text("Cancel", style: TextStyle(color: Colors.blueAccent)),
                  onPressed: () => Navigator.pop(context),
                )
                    : Container(),
                TextButton(
                  child: Text(widget.confirmText!, style: TextStyle(color: Colors.blueAccent)),
                  onPressed: () {
                    if (widget.needPassword || widget.needDescription) {
                      Navigator.pop(context, {"des": msgController.text, "pass": passController.text});
                    } else {
                      Navigator.pop(context, "---confirm---");
                    }
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  _buildMsgField() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextField(
        maxLines: null,
        controller: msgController,
        decoration: InputDecoration(
          hintText: widget.descriptionLabel,
          contentPadding: EdgeInsets.all(5),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.teal),
          ),
        ),
      ),
    );
  }

  _buildPasswordField() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextField(
        controller: passController,
        obscureText: !_showPassword,
        decoration: InputDecoration(
          hintText: "Password",
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                _showPassword = !_showPassword;
              });
            },
            icon: _showPassword ? Icon(Icons.remove_red_eye) : Icon(Icons.remove_red_eye_outlined),
            highlightColor: Colors.red,
          ),
          contentPadding: EdgeInsets.all(5),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.teal),
          ),
        ),
      ),
    );
  }
}
