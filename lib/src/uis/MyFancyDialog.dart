import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'Package:archive/archive_io.dart';

class MyFancyDialog extends StatefulWidget {
  final String title;
  final String body;
  final IconData? icon;
  final Color color;
  final Color iconColor;
  final double radius;
  final double iconSize;
  final double radiusSmall;
  final double buttonHeight;
  final List<String> actionsInRow;
  final List<String> actionsInCol;
  final List<String> inputs;
  final List<String> numberInputs;
  final List<String> multiLineInputs;
  final List<String> dateTimes;

  MyFancyDialog(
      {this.buttonHeight = 50,
        this.radiusSmall = 6,
        this.radius = 25,
        this.iconSize = 50,
        this.iconColor = Colors.white,
        this.actionsInCol = const ["Cancel"],
        this.actionsInRow = const ["Confirm"],
        this.title = "",
        this.body = "",
        this.icon,
        this.inputs = const [],
        this.numberInputs = const [],
        this.multiLineInputs = const [],
        this.dateTimes = const [],
        this.color = const Color(0xfffad82f)});

  @override
  _MyFancyDialogState createState() => _MyFancyDialogState();
}

class _MyFancyDialogState extends State<MyFancyDialog> {
  List<TextEditingController> inputControllers = [];
  List<FocusNode> inputFNS = [];
  List<DateTime> selectedDateTimes = [];

  @override
  void initState() {
    // TODO: implement initState
    inputControllers =
        (widget.inputs + widget.numberInputs + widget.multiLineInputs).map((e) => TextEditingController()).toList();
    selectedDateTimes = (widget.dateTimes).map((e) => DateTime.now()).toList();
    inputFNS = (widget.inputs + widget.numberInputs + widget.multiLineInputs).map((e) => FocusNode()).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Dialog(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(widget.radius), topRight: Radius.circular(widget.radius))),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(widget.radius), topRight: Radius.circular(widget.radius))),
        width: width * 0.8,
//        height: height * 0.5,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                color: widget.color,
                borderRadius:
                BorderRadius.only(topLeft: Radius.circular(widget.radius), topRight: Radius.circular(widget.radius)),
              ),
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
              child: Row(
                children: [
                  Icon(widget.icon ?? Icons.error, size: widget.iconSize, color: widget.iconColor),
                  SizedBox(width: 10),
                  Expanded(
                      child: Text(widget.title, style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold)))
                ],
              ),
            ),
            Divider(color: widget.color.withOpacity(0.8), height: 1),
            Container(
              constraints: BoxConstraints(minHeight: height / 5, minWidth: double.infinity, maxHeight: height * 0.3),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(widget.radiusSmall), bottomRight: Radius.circular(widget.radiusSmall)),
              ),
              padding: EdgeInsets.only(left: 4, right: 4, top: 14, bottom: 10),
              width: width,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Center(
                        child: Text(
                          widget.body,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: widget.body.length > 0 ? 10 : 0,
                    ),
                    Column(
                      children: widget.inputs
                          .map(
                            (e) => Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CupertinoTextField(
                            placeholder: e,
                            onSubmitted: (v) {
                              if (widget.inputs.indexOf(e) == widget.inputs.length - 1) {
                                if (widget.multiLineInputs.isNotEmpty || widget.numberInputs.isNotEmpty) {
                                  inputFNS[widget.inputs.indexOf(e) + 1].requestFocus();
                                }
                              } else {
                                inputFNS[widget.inputs.indexOf(e) + 1].requestFocus();
                              }
                            },
                            focusNode: inputFNS[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                            controller:
                            inputControllers[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                          ),
                        ),
                      )
                          .toList(),
                    ),
                    Column(
                      children: widget.numberInputs
                          .map(
                            (e) => Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CupertinoTextField(
                            keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
                            placeholder: e,
                            onSubmitted: (v) {
                              if (widget.numberInputs.indexOf(e) == widget.numberInputs.length - 1) {
                                if (widget.numberInputs.isNotEmpty) {
                                  inputFNS[widget.inputs.indexOf(e) + 1].requestFocus();
                                }
                              } else {
                                inputFNS[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e) + 1]
                                    .requestFocus();
                              }
                            },
                            focusNode: inputFNS[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                            controller:
                            inputControllers[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                          ),
                        ),
                      )
                          .toList(),
                    ),
                    Column(
                      children: widget.multiLineInputs
                          .map(
                            (e) => Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CupertinoTextField(
                            maxLines: null,
                            placeholder: e,
                            onSubmitted: (v) {
                              if (widget.multiLineInputs.indexOf(e) == widget.multiLineInputs.length - 1) {
                              } else {
                                inputFNS[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e) + 1]
                                    .requestFocus();
                              }
                            },
                            focusNode: inputFNS[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                            controller:
                            inputControllers[(widget.inputs + widget.numberInputs + widget.multiLineInputs).indexOf(e)],
                          ),
                        ),
                      )
                          .toList(),
                    ),
                    Column(
                      children: widget.dateTimes.map(
                            (e) {
                          int i = widget.dateTimes.indexOf(e);
                          return Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Row(
                              children: [
                                Text(e),
                                SizedBox(width: 5),
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.all(5),
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          color: Colors.black26,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: IconButton(
                                          icon: Icon(Icons.arrow_left, color: Colors.white),
                                          onPressed: () async {
                                            setState(() {
                                              selectedDateTimes[i]=selectedDateTimes[i].add(Duration(days: -1));
                                            });
                                          },
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.all(5),
                                          padding: EdgeInsets.all(5),
                                          height: 40,
                                          // width: 140,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color: Colors.black26,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: InkWell(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text(DateFormat.yMMMMd().format(selectedDateTimes[i]),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 10
                                                    )),
                                                Text(DateFormat.EEEE().format(selectedDateTimes[i]),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 10
                                                    )),
                                              ],
                                            ),
                                            onTap: () {
                                              showDatePicker(context: context, initialDate: selectedDateTimes[i], firstDate: DateTime(1900), lastDate: DateTime(2200)).then((value){
                                                if(value!=null){
                                                  print('confirm $value');
                                                  setState(() {
                                                    selectedDateTimes[i] = value;
                                                  });
                                                }
                                              });

//                            return showDialog(
//                                context: context,
//                                builder: (BuildContext context) =>
//                                    DatePickerDialog(model.flightsDate));
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(5),
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          color: Colors.black26,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: IconButton(
                                          icon: Icon(Icons.arrow_right, color: Colors.white),
                                          onPressed: () {
                                            setState(() {
                                              selectedDateTimes[i]= selectedDateTimes[i].add(Duration(days: 1));
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ).toList(),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: widget.actionsInRow.map((e) {
                bool isFirst = widget.actionsInRow.indexOf(e) == 0;
                bool isLast = widget.actionsInRow.indexOf(e) == widget.actionsInRow.length - 1;
                return Expanded(
                    child: TextButton(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.all(0),
                        fixedSize: Size.fromHeight(widget.buttonHeight),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(isFirst ? widget.radiusSmall : 0),
                                bottomLeft: Radius.circular(isFirst ? widget.radiusSmall : 0),
                                bottomRight: Radius.circular(isLast ? widget.radiusSmall : 0),
                                topRight: Radius.circular(isLast ? widget.radiusSmall : 0))),
                        backgroundColor:Colors.white,
                      ),
                      onPressed: () => Navigator.pop(
                          context, e == "Cancel" ? null : DialogResult(e, inputControllers.map((e) => e.text).toList(),selectedDateTimes)),
                      child: Text(e, style: TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.bold)),
                    ));
              }).toList(),
            ),
            Column(
              children: widget.actionsInCol.map((e) {
                return Column(
                  children: [
                    SizedBox(
                      width: width,
                      height: widget.buttonHeight,
                      child: TextButton(
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.all(0),
                          fixedSize: Size.fromHeight(widget.buttonHeight),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.radiusSmall)),
                          backgroundColor:Colors.white,
                        ),

                        onPressed: () => Navigator.pop(
                            context, e == "Cancel" ? null : DialogResult(e, inputControllers.map((e) => e.text).toList(),
                            selectedDateTimes)),
                        child: Text(e, style: TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.bold)),

                      ),
                    ),
                  ],
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }
}

class DialogResult {
  String action;
  List<String> inputs;
  List<DateTime> dates;

  DialogResult(this.action, this.inputs,this.dates);

  @override
  String toString() {
    // TODO: implement toString
    return "$action ${inputs.join("-")} ${dates.join("-")}";
  }
}
