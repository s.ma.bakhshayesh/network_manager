import 'package:dio/src/adapters/browser_adapter.dart';
import 'package:dio/dio.dart';

HttpClientAdapter getAdapter() {
  return BrowserHttpClientAdapter();
}