import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:network_manager/src/uis/MyFancyDialog.dart';

import 'uis/MyDialog.dart';
// import 'package:handy_ui_kit/handy_ui_kit.dart';

/// A Network Manager.
class NetworkManagerMetaData {
  /// have several http functions

  ///called on start every request
  Function onStartDefault = () {};

  ///called on End every request
  Function onEndDefault = () {};

  ///called on Success every request
  Function onSuccessDefault = () {};

  ///called on Failed every request
  Function onFailedDefault = () {};

  CancelToken? globalCancelToken;

  Future Function(BuildContext? context, String title, String body)?
  successDialog;
  Future Function(BuildContext? context, String title, String body)? failedDialog;

  ///String token which is used in header
  String? token;

  ///other rules for check beside result code 200 should be a function return bool value
  bool Function(dynamic responseJson) successRules =
      (dynamic responseJson) => true;

  ///conditions where token expires
  bool Function(dynamic responseJson) tokenExpireRules =
      (dynamic responseJson) => false;

  ///called on token expire
  void Function(void Function() retryFunctoin) onTokenExpire = (_) {};

  BaseOptions dioOption = Dio().options;

  /// a function to extract error as string
  String Function(dynamic error) errorMsgExtractor =
      (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  bool useFancyDialog = true;

  static final NetworkManagerMetaData _singleton =
  NetworkManagerMetaData._internal();

  NetworkManagerMetaData._internal();

  factory NetworkManagerMetaData() {
    return _singleton;
  }

  static setAccessToken(String token) {
    _singleton.token = token;
  }

  static initialize({
    String? baseUrl,
    Duration timeout = const Duration(milliseconds: 15000),
    String? token,
    Function? onStartDefault,
    Function? onEndDefault,
    Function? onSuccessDefault,
    Function? onFailedDefault,
    CancelToken? globalCancelToken,
    bool useFancyDialog = false,
    Map<String, dynamic>? headers,
    bool Function(dynamic responseJson)? successRules,
    bool Function(dynamic responseJson)? tokenExpireRules,
    required Future Function(BuildContext? context, String title, String body)
    successDialog,
    required Future Function(BuildContext? context, String title, String body)
    failedDialog,
    void Function(void Function() retryFunctoin)? onTokenExpire,
    String Function(dynamic error)? errorMsgExtractor,
    String Function(dynamic msg)? successMsgExtractor,
  }) {
    if (onStartDefault != null) {
      _singleton.onStartDefault = onStartDefault;
    }
    if (onEndDefault != null) {
      _singleton.onEndDefault = onEndDefault;
    }
    if (onSuccessDefault != null) {
      _singleton.onSuccessDefault = onSuccessDefault;
    }
    if (onFailedDefault != null) {
      _singleton.onFailedDefault = onFailedDefault;
    }
    if (successRules != null) {
      _singleton.successRules = successRules;
    }
    if (tokenExpireRules != null) {
      _singleton.tokenExpireRules = tokenExpireRules;
    }
    if (onTokenExpire != null) {
      _singleton.onTokenExpire = onTokenExpire;
    }
    if (baseUrl != null) {
      _singleton.dioOption.baseUrl = baseUrl;
    }
    if (headers != null) {
      _singleton.dioOption.headers = headers;
    }
      _singleton.dioOption.connectTimeout = timeout;
      _singleton.dioOption.sendTimeout = timeout;
      _singleton.dioOption.receiveTimeout = timeout;
      _singleton.successDialog = successDialog;
      _singleton.failedDialog = failedDialog;
      _singleton.useFancyDialog = useFancyDialog;

    if (globalCancelToken != null) {
      _singleton.globalCancelToken = globalCancelToken;
    }

    _singleton.token = token;

    ///You can set LogInterceptor to print request/response log automaticlly
//    _singleton.dio.interceptors.add(LogInterceptor(responseBody: false));
    if (errorMsgExtractor != null) {
      _singleton.errorMsgExtractor = errorMsgExtractor;
    }
    if (successMsgExtractor != null) {
      _singleton.successMsgExtractor = errorMsgExtractor;
    }
  }
}

class NetworkManager {
  ///need context to show dialog or pop and push
  BuildContext? context;

  ///called on start this instance of Network Manager request
  Function? onStart = () {};

  ///called on end this instance of Network Manager request
  Function? onEnd = () {};

  ///called on success this instance of Network Manager request
  late void Function(BuildContext? context, NMResponse responseJson) onSuccess;

  ///called on failed this instance of Network Manager request
  late Function(BuildContext? context, NMResponse responseJson) onFailed =
      (BuildContext? context, NMResponse responseJson) {};

  ///check to show dialog when when request is success
  bool? showFailedDialog = false;

  ///check to show dialog when when request is failed
  bool? showSuccessDialog = false;

  ///check to call default functions each step
  bool? callDefaults = true;

  bool? useFancyDialog = true;

  ///api address you're requesting
  String? api;

  ///dynamic request data for post methods
  dynamic req;

  ///Map<String dynamic> query data for get methods
  Map<String, dynamic>? query;

  /// dio instance for making requests
  Dio? dio = Dio()
    ..options = BaseOptions(headers: {'Content-Type': 'application/json'});

  Function? onStartDefault = () {};

  ///called on End every request
  Function? onEndDefault = () {};

  Future Function(BuildContext? context, String title, String body)?
  successDialog;
  Future Function(BuildContext? context, String title, String body)? failedDialog;

  ///called on Success every request
  Function? onSuccessDefault = () {};

  ///called on Failed every request
  Function? onFailedDefault = () {};

  ///String token which is used in header
  String? token = "";

  ///other rules for check beside result code 200 should be a function return bool value
  bool Function(dynamic responseJson)? successRules =
      (dynamic responseJson) => true;

  ///conditions where token expires
  bool Function(dynamic responseJson)? tokenExpireRules =
      (dynamic responseJson) => false;


  bool forbidCancelWhenOffline;
  ///called on token expire
  void Function(void Function() retryFunctoin)? onTokenExpire = (_) {};

  BaseOptions dioOption = Dio().options;

  /// a function to extract error as string
  String Function(dynamic error)? errorMsgExtractor =
      (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  CancelToken? cancelToken;

  NetworkManager._(
      {this.showSuccessDialog,
        this.callDefaults,
        this.onStartDefault,
        this.onEndDefault,
        this.onFailedDefault,
        this.successRules,
        this.successMsgExtractor,
        this.onSuccessDefault,
        this.dio,
        this.useFancyDialog,
        this.errorMsgExtractor,
        this.showFailedDialog,
        this.token,
        this.req,
        this.context,
        this.api,
        required this.onSuccess,
        this.onEnd,
        required this.onFailed,
        this.query,
        this.successDialog,
        this.failedDialog,
        this.onStart,
        this.cancelToken,
        this.onTokenExpire,
        this.forbidCancelWhenOffline=false,
        this.tokenExpireRules});

  factory NetworkManager.instance({
    required BuildContext context,
    required String api,
    dynamic req,
    Map<String, dynamic>? query,
    void Function(BuildContext? context, NMResponse result)? onSuccess,
    void Function(BuildContext? context, NMResponse result)? onFailed,
    Function? onStart,
    Function? onEnd,
    Duration? timeout,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
    bool showFailedDialog = true,
    bool callDefaults = true,
    bool followRedirect = true,
    bool Function(int?)? validStatus,
    bool showSuccessDialog = false,
    bool forbidCancelWhenOffline = false,
  }) {
    NetworkManagerMetaData meta = NetworkManagerMetaData();

    BaseOptions finalOptions = meta.dioOption;
    if (headers != null) {
      finalOptions.headers = headers;
    }
    if (timeout != null) {
      finalOptions.connectTimeout = timeout;
      finalOptions.sendTimeout = timeout;
      finalOptions.receiveTimeout = timeout;
    }
      finalOptions.followRedirects = followRedirect;
    if (validStatus != null) {
      finalOptions.validateStatus = validStatus;
    }



    return NetworkManager._(
      context: context,
      api: api,
      req: req,
      query: query,
      onSuccess: onSuccess ?? (BuildContext? context, NMResponse responseJson) {},
      onFailed: onFailed ?? (BuildContext? context, NMResponse responseJson) {},
      onStart: onStart ?? () {},
      onEnd: onEnd ?? () {},
      cancelToken: cancelToken ?? meta.globalCancelToken,
      showFailedDialog: showFailedDialog,
      callDefaults: callDefaults,
      showSuccessDialog: showSuccessDialog,
      onSuccessDefault: meta.onSuccessDefault,
      onFailedDefault: meta.onFailedDefault,
      onStartDefault: meta.onStartDefault,
      onEndDefault: meta.onEndDefault,
      token: meta.token,
      successRules: meta.successRules,
      tokenExpireRules: meta.tokenExpireRules,
      onTokenExpire: meta.onTokenExpire,
      successMsgExtractor: meta.successMsgExtractor,
      errorMsgExtractor: meta.errorMsgExtractor,
      successDialog: meta.successDialog,
      useFancyDialog: meta.useFancyDialog,
      failedDialog: meta.failedDialog,
      forbidCancelWhenOffline: forbidCancelWhenOffline,
      dio: Dio()..options = finalOptions,
    );
  }

  _requestPost() {
    if (req is Map<String, dynamic> && req.keys.contains("Token")) {
      req["Token"] = NetworkManagerMetaData().token;
    }
    if (callDefaults!) {
      onStartDefault!();
    }
    onStart!();
    log("Posting ${jsonEncode(req)} in ${jsonEncode(api)}");
    try {
      dio!.post(api!, data: req, cancelToken: cancelToken).then((response) {
        print("response : " + response.toString());
        var statusCode = response.statusCode;
        var resultJson;
        if (response.data != null) {
          if (response.data.runtimeType == String) {
            resultJson = jsonDecode(response.data);
          } else {
            resultJson = Map<String, dynamic>.from(response.data);
          }
          print("***Status Code*** : $statusCode");
//        print("***Response Data*** : $resultJson");
          log("***Response Data*** : ${jsonEncode(resultJson)}");
          if (statusCode == 200) {
            if (successRules!(resultJson)) {
              if (callDefaults!) {
                onSuccessDefault!();
              }
              NMResponse nmResponse = NMResponse(
                  responseTitle: "Success",
                  responseCode: statusCode,
                  responseBody: "",
                  isSucceed: true,
                  data: resultJson);

              onSuccess(context, nmResponse);
              if (showSuccessDialog! && context != null) {
                if (successDialog == null) {
                  showDialog(
                      context: context!,
                      builder: (context) => useFancyDialog!
                          ? MyFancyDialog(
                        title: "Success Operation",
                        body: successMsgExtractor!(resultJson),
                        icon: Icons.check_circle,
                        actionsInCol: ["OK"],
                        color: Colors.green,
                      )
                          : MyDialog(
                        title: "Success Operation",
                        body: successMsgExtractor!(resultJson),
                        type: DialogType.success,
                      ));
                } else {
                  successDialog!(context, "Success Operation",
                      successMsgExtractor!(resultJson))
                      .then((value) {});
                }
              }
            } else {
              if (callDefaults!) {
                onFailedDefault!();
              }
              if (tokenExpireRules!(resultJson)) {
                onTokenExpire!(requestPost);
              } else {
                NMResponse nmResponse = NMResponse(
                    responseTitle: "Operation Failed",
                    responseCode: statusCode,
                    responseBody: errorMsgExtractor!(resultJson),
                    isSucceed: false,
                    retryFunction: requestPost,
                    data: resultJson);

                onFailed(context, nmResponse);
                if (showFailedDialog! && context != null) {
                  if (failedDialog == null) {
                    showDialog(
                        context: context!,
                        builder: (context) => useFancyDialog!
                            ? MyFancyDialog(
                          title: "Failed Operation",
                          body: errorMsgExtractor!(resultJson),
                          actionsInRow: ["Cancel", "Retry"],
                          actionsInCol: [],
                        )
                            : MyDialog(
                          title: "Failed Operation",
                          body: errorMsgExtractor!(resultJson),
                          type: DialogType.warning,
                          confirmText: "Retry",
                        )).then((value) {
                      print("dialog returned: " + value.toString());
                      if (value != null) {
                        _requestPost();
                      }
                    });
                  } else {
                    failedDialog!(context, "Failed Operation",
                        errorMsgExtractor!(resultJson))
                        .then((value) {
                      print("dialog returned: " + value.toString());
                      if (value != null) {
                        _requestPost();
                      }
                    });
                  }
                }
              }
            }
          } else {
            if (callDefaults!) {
              onFailedDefault!();
            }
            NMResponse nmResponse = NMResponse(
                responseTitle: "Operation Failed",
                responseCode: statusCode,
                responseBody: "Status Code Failure ($statusCode}",
                isSucceed: false,
                retryFunction: requestPost,
                data: resultJson);

            onFailed(context, nmResponse);
            if (showFailedDialog! && context != null) {
              if (failedDialog == null) {
                showDialog(
                    barrierDismissible: false,
                    context: context!,
                    builder: (context) => useFancyDialog!
                        ? MyFancyDialog(
                      title: "($statusCode) Connection Interrupted",
                      body: resultJson.toString(),
                      color: Colors.red,
                      actionsInRow:forbidCancelWhenOffline? ["Retry"]:["Cancel","Retry"],
                      actionsInCol: [],
                    )
                        : MyDialog(
                      title: "($statusCode) Connection Interrupted",
                      body: resultJson.toString(),
                      hasCancel: !forbidCancelWhenOffline,
                      type: DialogType.error,
                      confirmText: "Retry",
                    )).then((value) {
                  print("dialog returned: " + value.toString());
                  if (value != null) {
                    _requestPost();
                  }
                });
              } else {
                failedDialog!(context, "($statusCode) Connection Interrupted",
                    resultJson.toString())
                    .then((value) {
                  print("dialog returned: " + value.toString());
                  if (value != null) {
                    _requestPost();
                  }
                });
              }
            }
          }
          if (callDefaults!) {
            onEndDefault!();
          }
          onEnd!();
        }
      }).catchError((e) {
        if (e is DioError) {
          DioError ee = e;
//        print("Error: ${ee.error}");
          String errStr = e.toString();
//        print("Error: ${ee.error}");
          log("Error:${ee.toString()}");

          switch (ee.type) {
            case DioExceptionType.badResponse:
              String? msg = ee.response!.data == null
                  ? "Unknown Error"
                  : ee.message ;
              errStr = "Something Went Wrong (${ee.response!.statusCode??-500})!\n $msg";
              break;
            case DioExceptionType.unknown:
              errStr = "Network Error.\n Connection Interrupted!";
              break;
            case DioExceptionType.cancel:
              errStr = "Request is Canceled";
              break;
            case DioExceptionType.connectionTimeout:
              errStr = "Connection Timeout!";
              break;
            case DioExceptionType.receiveTimeout:
              errStr = "Connection Timeout while Receiving Response!";
              break;
            case DioExceptionType.sendTimeout:
              errStr = "Connection Timeout while Sending Request!";
              break;
            default:
              errStr = "Unknown Error!\nSomething went Wrong!";
          }
          log("Error:${ee.toString()} : $errStr ");
          if (callDefaults!) {
            onFailedDefault!();
            onEndDefault!();
          }
          NMResponse nmResponse = NMResponse(
              responseTitle: "Operation Failed",
              responseCode: ee.type == DioExceptionType.badResponse
                  ? ee.response!.statusCode
                  : -10000,
              responseBody: errStr,
              isSucceed: false,
              retryFunction: requestPost,
              data: ee);

          onFailed(context, nmResponse);
          onEnd!();
          if (showFailedDialog! && context != null) {
            if (failedDialog == null) {
              print("forbid $forbidCancelWhenOffline");
              bool hasCancelButton = !errStr.contains("Network Error") || !forbidCancelWhenOffline;
              print("has cancel $hasCancelButton");
              showDialog(
                  context: context!,
                  barrierDismissible: hasCancelButton,
                  builder: (context) => useFancyDialog!
                      ? MyFancyDialog(
                    title: "Failed Operation",
                    body: errStr,
                    color: Colors.red,
                    actionsInRow:!hasCancelButton?["Retry"]: ["Cancel", "Retry"],
                    actionsInCol: [],
                  )
                      : MyDialog(
                    title: "Failed Operation",
                    body: errStr,
                    hasCancel: hasCancelButton,
                    type: DialogType.error,
                    confirmText: "Retry",
                  )).then((value) {
                print("dialog returned: " + value.toString());
                if (value != null) {
                  _requestPost();
                }
              });
            } else {
              failedDialog!(context, "Failed Operation", errStr).then((value) {
                print("dialog returned: " + value.toString());
                if (value != null) {
                  _requestPost();
                }
              });
            }
          }
        } else {
          print("Error: $e");
          log("Error:${e.toString()} : -2");
          if (callDefaults!) {
            onFailedDefault!();
            onEndDefault!();
          }
          NMResponse nmResponse = NMResponse(
              responseTitle: "Operation Failed",
              responseCode: -11111,
              responseBody: "Flutter layer Failure",
              isSucceed: false,
              retryFunction: requestPost,
              data: e);

          onFailed(context, nmResponse);
          onEnd!();
          if (showFailedDialog! && context != null) {
            if (failedDialog == null) {
              showDialog(
                  context: context!,
                  builder: (context) => useFancyDialog!
                      ? MyFancyDialog(
                    title: "Failed Operation",
                    body: e.toString(),
                    color: Colors.red,
                    actionsInRow: ["Cancel", "Retry"],
                    actionsInCol: [],
                  )
                      : MyDialog(
                    title: "Failed Operation",
                    body: e.toString(),
                    type: DialogType.error,
                    confirmText: "Retry",
                  )).then((value) {
                print("dialog returned: " + value.toString());
                if (value != null) {
                  _requestPost();
                }
              });
            } else {
              failedDialog!(context, "Failed Operation", e.toString())
                  .then((value) {
                print("dialog returned: " + value.toString());
                if (value != null) {
                  _requestPost();
                }
              });
            }
          }
        }
      });
    } on Exception catch (e) {
      print('error caught: $e');
      String errStr = e.toString();
      if (callDefaults!) {
        onFailedDefault!();
        onEndDefault!();
      }
      NMResponse nmResponse = NMResponse(
          responseTitle: "Operation Failed",
          responseCode: -22222,
          responseBody: "Exception Occurred",
          isSucceed: false,
          retryFunction: requestPost,
          data: e);

      onFailed(context, nmResponse);
      onEnd!();
      if (e.toString().contains("SocketException")) {
        errStr = "No Internet";
      }
      if (showFailedDialog! && context != null) {
        if (failedDialog == null) {
          showDialog(
              context: context!,
              builder: (context) => useFancyDialog!
                  ? MyFancyDialog(
                title: "Failed Operation",
                body: errStr,
                color: Colors.red,
                actionsInRow: ["Cancel", "Retry"],
                actionsInCol: [],
              )
                  : MyDialog(
                title: "Failed Operation",
                body: errStr,
                type: DialogType.error,
                confirmText: "Retry",
              )).then((value) {
            print("dialog returned: " + value.toString());
            if (value != null) {
              _requestPost();
            }
          });
        } else {
          failedDialog!(context, "Failed Operation", errStr).then((value) {
            print("dialog returned: " + value.toString());
            if (value != null) {
              _requestPost();
            }
          });
        }
      }
    }
  }

  _requestGet() {
    if (callDefaults!) {
      onStartDefault!();
    }
    onStart!();
    print("Getting $api with $query");
    dio!.get(api!, queryParameters: req).then((response) {
      var statusCode = response.statusCode;
      var resultJson = response.data;
      print("***Status Code*** : $statusCode");
      print("***Response Data*** : $resultJson");
      if (statusCode == 200 && successRules!(resultJson)) {
        if (callDefaults!) {
          onSuccessDefault!();
        }
        NMResponse nmResponse = NMResponse(
            responseTitle: "Success",
            responseCode: statusCode,
            responseBody: "",
            isSucceed: true,
            data: resultJson);

        onSuccess(context, nmResponse);
      } else {
        if (callDefaults!) {
          onFailedDefault!();
        }
        NMResponse nmResponse = NMResponse(
            responseTitle: "Failed",
            responseCode: statusCode,
            responseBody: "",
            isSucceed: false,
            retryFunction: requestPost,
            data: resultJson);

        onFailed(context, nmResponse);
      }
      if (callDefaults!) {
        onEndDefault!();
      }
      onEnd!();
    }).catchError((e) {
      if (callDefaults!) {
        onFailedDefault!();
        onEndDefault!();
      }
      print("Error: $e");
      NMResponse nmResponse = NMResponse(
          responseTitle: "Failed",
          responseCode: -11111,
          responseBody: "",
          isSucceed: false,
          retryFunction: requestPost,
          data: e);

      onFailed(context, nmResponse);
      onEnd!();
    });
  }

  get requestPost => _requestPost;

  get requestGet => _requestGet;
}

class NetworkManagerAsync {
  ///api address you're requesting
  String? api;

  ///dynamic request data for post methods
  dynamic req;

  ///Map<String dynamic> query data for get methods
  Map<String, dynamic>? query;

  /// dio instance for making requests
  Dio? dio = Dio()
    ..options = BaseOptions(headers: {'Content-Type': 'application/json'});

  ///other rules for check beside result code 200 should be a function return bool value
  bool Function(dynamic responseJson)? successRules =
      (dynamic responseJson) => true;

  ///conditions where token expires
  bool Function(dynamic responseJson)? tokenExpireRules =
      (dynamic responseJson) => false;

  ///called on token expire
  void Function(void Function() retryFunctoin)? onTokenExpire = (_) {};

  BaseOptions dioOption = Dio().options;

  /// a function to extract error as string
  String Function(dynamic error)? errorMsgExtractor =
      (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  CancelToken? cancelToken;

  NetworkManagerAsync._(
      {this.successRules,
        this.successMsgExtractor,
        this.dio,
        this.errorMsgExtractor,
        this.req,
        this.api,
        this.query,
        this.cancelToken,
        this.onTokenExpire,
        this.tokenExpireRules});

  factory NetworkManagerAsync.instance({
    required String api,
    required dynamic req,
    Map<String, dynamic>? query,
    CancelToken? cancelToken,
    Map<String, dynamic>? headers,
  }) {
    NetworkManagerMetaData meta = NetworkManagerMetaData();

    BaseOptions finalOptions = meta.dioOption;
    if (headers != null) {
      finalOptions.headers = headers;
    }

    return NetworkManagerAsync._(
      api: api,
      req: req,
      query: query,
      cancelToken: cancelToken,
      successRules: meta.successRules,
      tokenExpireRules: meta.tokenExpireRules,
      onTokenExpire: meta.onTokenExpire,
      successMsgExtractor: meta.successMsgExtractor,
      errorMsgExtractor: meta.errorMsgExtractor,
      dio: Dio()..options = finalOptions,
    );
  }

  Future<NMResponse> _requestPostWithResponse() async {
    NMResponse nmResponse = NMResponse();
    log("Posting ${jsonEncode(req)} in ${jsonEncode(api)}");
    try {
      dynamic response = await dio!
          .post(api!, data: req, cancelToken: cancelToken)
          .catchError((e) {
        if (e is DioException) {
          DioException ee = e;
          String errStr = e.toString();

          switch (ee.type) {
            case DioExceptionType.unknown:
              errStr =
              "Something Went Wrong (${ee.response!.statusCode})!\n${ee.response!.data["Message"] ?? "Unknown Error"}!";
              break;
            case DioExceptionType.connectionTimeout:
              errStr = "Network Error.\n Connection Interrupted!";
              break;
            case DioExceptionType.cancel:
              errStr = "Request is Canceled";
              break;
            case DioExceptionType.connectionTimeout:
              errStr = "Connection Timeout!";
              break;
            case DioExceptionType.receiveTimeout:
              errStr = "Connection Timeout while Receiving Response!";
              break;
            case DioExceptionType.sendTimeout:
              errStr = "Connection Timeout while Sending Request!";
              break;
            default:
              errStr = "Unknown Error!\nSomething went Wrong!";
          }
          log("Error:${ee.toString()}: $errStr");
          nmResponse.responseCode = ee.response as int? ?? -1;
          nmResponse.data = ee;
          nmResponse.isSucceed = false;
          nmResponse.responseTitle = "Failed";
          nmResponse.responseBody = errStr;
        } else {
          print("Error: $e");
          nmResponse.responseCode = -11111;
          nmResponse.data = e;
          nmResponse.isSucceed = false;
          nmResponse.responseTitle = "Failed";
          nmResponse.responseBody = e.toString();
        }
        if (nmResponse.isSucceed!) {
        } else {
          throw Exception(nmResponse);
        }
      });
      var statusCode = response.statusCode;
      var resultJson;
      if (response != null && response.data != null) {
        if (response.data.runtimeType == String) {
          resultJson = jsonDecode(response.data);
        } else {
          resultJson = Map<String, dynamic>.from(response.data);
        }
        print("***Status Code*** : $statusCode");
        log("***Response Data*** : ${jsonEncode(resultJson)}");
        if (statusCode == 200) {
          if (successRules!(resultJson)) {
            nmResponse.responseCode = statusCode;
            nmResponse.data = resultJson;
            nmResponse.isSucceed = true;
            nmResponse.responseTitle = "Success";
            nmResponse.responseBody = successMsgExtractor!(resultJson);
          } else {
            if (tokenExpireRules!(resultJson)) {
              nmResponse.responseCode = -999;
              nmResponse.data = resultJson;
              nmResponse.isSucceed = false;
              nmResponse.responseTitle = "Token Expire";
              nmResponse.responseBody = "Token Expire";
            } else {
              nmResponse.responseCode = statusCode;
              nmResponse.data = resultJson;
              nmResponse.isSucceed = false;
              nmResponse.responseTitle = "Failed";
              nmResponse.responseBody = errorMsgExtractor!(resultJson);
            }
          }
        } else {
          nmResponse.responseCode = statusCode;
          nmResponse.data = resultJson;
          nmResponse.isSucceed = false;
          nmResponse.responseTitle = "Failed";
          nmResponse.responseBody = "Connection Interrupted";
        }
      }
    } on Exception catch (e) {
      print('error caught: $e');
      nmResponse.responseCode = -11111;
      nmResponse.data = e;
      nmResponse.isSucceed = false;
      nmResponse.responseTitle = "Failed";
      nmResponse.responseBody = e.toString();
    }
    if (nmResponse.isSucceed!) {
      return nmResponse;
    } else {
      throw Exception(nmResponse);
    }
  }

  get requestPostAsync => _requestPostWithResponse;
}

class NMResponse {
  bool? isSucceed;
  int? responseCode;
  String? responseTitle;
  String? responseBody;
  Function? retryFunction = () {};
  dynamic data;

  NMResponse(
      {this.data,
        this.isSucceed,
        this.responseBody,
        this.responseCode,
        this.responseTitle,
        this.retryFunction});

  @override
  String toString() {
    return "($responseCode) $responseTitle \n $responseBody";
  }
}


bool isJson(String jsonString){
  var decodeSucceeded = false;
  try {
    // var decodedJSON = json.decode(jsonString) as Map<String, dynamic>;
    decodeSucceeded = true;
  } on FormatException catch (e) {
    print('The provided string is not valid JSON\n$e');
  }
  return decodeSucceeded;
}