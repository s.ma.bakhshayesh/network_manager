import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:convert/convert.dart';
import 'package:dio/io.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'Package:archive/archive_io.dart';
import './adapter_selector.dart';

class NetworkOption extends BaseOptions {
  ///called on start every request
  Function(NetworkRequest) onStartDefault = (NetworkRequest request) {};

  ///called on End every request
  Function(NetworkResponse, NetworkRequest) onEndDefault = (NetworkResponse response, NetworkRequest request) {};

  ///called on Success every request
  void Function(NetworkResponse, NetworkRequest) onSuccessDefault = (NetworkResponse response, NetworkRequest request) {};

  ///called on Failed every request
  void Function(NetworkResponse, NetworkRequest) onFailedDefault = (NetworkResponse response, NetworkRequest request) {};

  CancelToken? globalCancelToken;

  ///String token which is used in header
  String? token;

  ///called on token expire
  bool Function(NetworkResponse, NetworkRequest) tokenExpireRule = (res, _) => res.responseCode == 403;
  bool Function(NetworkResponse, NetworkRequest) extraSuccessRule = (res, _) => true;
  void Function(NetworkResponse, NetworkRequest) onTokenExpire = (res, _) => print("Token Expired");

  /// a function to extract error as string
  String Function(dynamic error) errorMsgExtractor = (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  static final NetworkOption _singleton = NetworkOption._internal();

  NetworkOption._internal();

  factory NetworkOption() {
    return _singleton;
  }

  Options? get option => Options(
        receiveTimeout: receiveTimeout,
        sendTimeout: sendTimeout,
        extra: extra,
        headers: headers,
        followRedirects: followRedirects,
        contentType: contentType,
      );

  static setAccessToken(String token, {String tokenKey = "Authorization", String tokenPrefix = "Bearer "}) {
    log("Token is set: $token");
    _singleton.headers.update('Content-Type', (v) => "application/json; charset=UTF-8");
    //     ifAbsent: () => _singleton.headers.putIfAbsent("$tokenKey", () => "$tokenPrefix$token"));
    _singleton.token = token;
  }

  static initialize({
    String? baseUrl,
    Duration timeout = const Duration(milliseconds: 15000),
    String? token,
    bool? followRedirects,
    void Function(NetworkRequest)? onStartDefault,
    void Function(NetworkResponse, NetworkRequest)? onEndDefault,
    Function(NetworkResponse, NetworkRequest)? onSuccessDefault,
    Function(NetworkResponse, NetworkRequest)? onFailedDefault,
    CancelToken? globalCancelToken,
    Map<String, dynamic>? headers,
    bool Function(NetworkResponse, NetworkRequest)? tokenExpireRule,
    bool Function(NetworkResponse, NetworkRequest)? extraSuccessRule,
    void Function(NetworkResponse, NetworkRequest)? onTokenExpire,
    String Function(dynamic error)? errorMsgExtractor,
    String Function(dynamic msg)? successMsgExtractor,
  }) {
    if (onStartDefault != null) {
      _singleton.onStartDefault = onStartDefault;
    }
    if (onEndDefault != null) {
      _singleton.onEndDefault = onEndDefault;
    }
    if (onSuccessDefault != null) {
      _singleton.onSuccessDefault = onSuccessDefault as void Function(NetworkResponse, NetworkRequest);
    }
    if (onFailedDefault != null) {
      _singleton.onFailedDefault = onFailedDefault as void Function(NetworkResponse, NetworkRequest);
    }
    if (onTokenExpire != null) {
      _singleton.onTokenExpire = onTokenExpire;
    }
    if (tokenExpireRule != null) {
      _singleton.tokenExpireRule = tokenExpireRule;
    }
    if (extraSuccessRule != null) {
      _singleton.extraSuccessRule = extraSuccessRule;
    }
    if (baseUrl != null) {
      _singleton.baseUrl = baseUrl;
    }
    if (headers != null) {
      _singleton.headers = headers;
    }
    _singleton.connectTimeout = timeout;
    _singleton.sendTimeout = timeout;
    _singleton.receiveTimeout = timeout;

    if (globalCancelToken != null) {
      _singleton.globalCancelToken = globalCancelToken;
    }

    if (followRedirects != null) {
      _singleton.followRedirects = followRedirects;
    }

    _singleton.token = token;

    ///You can set LogInterceptor to print request/response log automatically
//    _singleton.dio.interceptors.add(LogInterceptor(responseBody: false));
    if (errorMsgExtractor != null) {
      _singleton.errorMsgExtractor = errorMsgExtractor;
    }
    if (successMsgExtractor != null) {
      _singleton.successMsgExtractor = errorMsgExtractor;
    }
  }
}

class NetworkResponse {
  bool responseStatus;
  bool isCancelable;
  bool callDefaults;
  bool handleErrors;
  bool handleSuccess;
  int responseCode;
  String? responseTitle;
  dynamic Function(bool force,NetworkResponse res)? retryFunction;
  dynamic responseBody;
  dynamic responseDetails;
  String? extractedMessage;

  NetworkResponse(
      {required this.responseStatus,
      required this.responseCode,
      this.responseTitle,
      this.responseBody,
      this.isCancelable = true,
      this.callDefaults = true,
      this.handleErrors = true,
      this.handleSuccess = true,
      this.retryFunction,
      this.responseDetails,
      this.extractedMessage});

  @override
  String toString() {
    return "$responseCode $responseTitle : $extractedMessage";
  }
}

HttpClient createMyHttpClient() {
  return HttpClient()..idleTimeout = const Duration(seconds: 300);
}

class NetworkRequest {
  final String api;
  final String? savePath;
  final dynamic data;
  final Dio dio;
  final Duration timeOut;
  final NetworkOption options;
  final bool callDefaults;
  final bool handleErrors;
  final bool handleSuccess;
  final bool isCancelable;
  final bool useCompression;
  final String compressDataKey;

  final dynamic Function(bool force,NetworkResponse response)? retry;
  final Function(int, int)? onReceiveProgress;

  NetworkRequest._({
    required this.api,
    this.data,
    this.timeOut = const Duration(seconds: 30),
    required this.options,
    this.callDefaults = true,
    this.handleErrors = true,
    this.handleSuccess = true,
    this.isCancelable = true,
    this.useCompression = false,
    this.retry,
    this.compressDataKey = "Body",
    this.onReceiveProgress,
    this.savePath,
  }) : dio = Dio(options);

  factory NetworkRequest({
    required api,
    data,
    bool callDefaults = true,
    timeOut = const Duration(seconds: 30),
    bool handleErrors = true,
    bool handleSuccess = true,
    dynamic Function(bool force,NetworkResponse response)? retry,
    void Function(int, int)? onReceiveProgress,
    String? savePath,
    String compressDataKey = "Body",
    bool useCompression = false,
  }) {
    NetworkOption option = NetworkOption();

    return NetworkRequest._(
      api: api,
      data: data,
      timeOut: timeOut,
      useCompression: useCompression,
      compressDataKey: compressDataKey,
      options: option,
      callDefaults: callDefaults,
      handleErrors: handleErrors,
      handleSuccess: handleSuccess,
      retry: retry,
      onReceiveProgress: onReceiveProgress,
      savePath: savePath,
    );
  }

  Future<NetworkResponse> post() async {
    if (callDefaults) {
      options.onStartDefault(this);
    }

    // NetworkConfig config = get();
    HttpClientAdapter client =  getAdapter();

    // this.dio.httpClientAdapter = client;
    // ..interceptors.addAll(config.interceptors);
    dio.httpClientAdapter = client;
    // dio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: createMyHttpClient);
    NetworkResponse response;
    try {
      log("Posting ${jsonEncode(data)} in ${jsonEncode(api)}");
      Response res = await dio.post(api, data: data, cancelToken: options.globalCancelToken, options: options.option).catchError((e) {
        if(handleErrors) {
          throw e;
        }
      }).timeout(timeOut, onTimeout:handleErrors? () {
          throw DioExceptionType.connectionTimeout;
      }:null);
      bool isJson = res.headers.value("content-type")!.contains("json");
      if (!isJson && !useCompression) {
        res.data = jsonDecode(res.data);
      }
      res.data = decompressData(res.data, useCompression, compressDataKey: compressDataKey);
      response = NetworkResponse(
          responseStatus: res.statusCode! > 199 && res.statusCode! < 300,
          responseCode: res.statusCode!,
          responseTitle: res.statusMessage,
          responseBody: res.data,
          responseDetails: res,
          extractedMessage: options.successMsgExtractor!(res.data));
    } catch (e) {
      if (e is DioException && e.response != null && e.response!.data is Map) {
        response = NetworkResponse(
            responseStatus: e.response!.statusCode! > 199 && e.response!.statusCode! < 300,
            responseCode: e.response!.statusCode!,
            responseTitle: e.response!.statusMessage,
            responseBody: e.response!.data,
            responseDetails: e,
            extractedMessage: options.errorMsgExtractor(e.response!.data));
      } else if (e is DioException && e.type == DioExceptionType.badResponse) {
        response = NetworkResponse(responseStatus: false, responseCode: e.response!.statusCode!, responseTitle: e.response!.statusMessage, responseBody: e.response!.data, responseDetails: e, extractedMessage: "${e.message}");
      } else if (e is DioException) {
        if (e.type == DioExceptionType.unknown) {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        } else if (e.type == DioExceptionType.connectionTimeout || e.type == DioExceptionType.receiveTimeout || e.type == DioExceptionType.sendTimeout) {
          response = NetworkResponse(
              responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection Timeout ,Can not Reach to Server!", responseDetails: e, extractedMessage: "Connection Timeout ,Can not Reach to Server!");
        } else if (e.type == DioErrorType.cancel) {
          response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Request Canceled!", responseDetails: e, extractedMessage: "Request Canceled!");
        } else {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        }
      } else {
        response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Failed", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: e.toString());
      }
    }

    response.isCancelable = isCancelable;
    response.callDefaults = callDefaults;
    response.handleSuccess = handleSuccess;
    response.handleErrors = handleErrors;

    response.retryFunction = retry ?? (_,response) => post.call();
    log(response.toString());

    if (options.tokenExpireRule(response, this)) {
      options.onTokenExpire(response, this);
      response.extractedMessage = "Token Expired!";
      response.responseCode = -999;
      response.responseStatus = false;
    }

    if (!options.extraSuccessRule(response, this)) {
      response.responseStatus = false;
      response.responseCode = -100;
      response.responseTitle = "Success Rules Failed";
    }

    if (callDefaults) {
      if (response.responseStatus) {
        options.onSuccessDefault(response, this);
      } else {
        options.onFailedDefault(response, this);
      }
      options.onEndDefault(response, this);
    } else {
      if (response.responseStatus) {
        if (handleSuccess) {
          options.onSuccessDefault(response, this);
        }
      } else {
        if (handleErrors) {
          options.onFailedDefault(response, this);
        }
      }
    }

    return response;
  }

  Future<NetworkResponse> get() async {
    if (callDefaults) {
      options.onStartDefault(this);
    }
    NetworkResponse response;
    try {
      String query = api + data;
      log("Getting ${jsonEncode(query)}");
      Response res = await dio.get(query, cancelToken: options.globalCancelToken, options: options.option).catchError((e) {
        throw e;
      }).timeout(timeOut, onTimeout: () {
        throw DioExceptionType.connectionTimeout;
      });

      bool isJson = res.headers.value("content-type")!.contains("json");
      if (!isJson && !useCompression) {
        res.data = jsonDecode(res.data);
      }
      res.data = decompressData(res.data, useCompression, compressDataKey: compressDataKey);
      response = NetworkResponse(
          responseStatus: res.statusCode! > 199 && res.statusCode! < 300,
          responseCode: res.statusCode!,
          responseTitle: res.statusMessage,
          responseBody: res.data,
          responseDetails: res,
          extractedMessage: options.successMsgExtractor!(res.data));
    } catch (e) {
      if (e is DioError && e.response != null && e.response!.data is Map) {
        response = NetworkResponse(
            responseStatus: e.response!.statusCode! > 199 && e.response!.statusCode! < 300,
            responseCode: e.response!.statusCode!,
            responseTitle: e.response!.statusMessage,
            responseBody: e.response!.data,
            responseDetails: e,
            extractedMessage: options.errorMsgExtractor(e.response!.data));
      } else if (e is DioException && e.type == DioExceptionType.badResponse) {
        response = NetworkResponse(responseStatus: false, responseCode: e.response!.statusCode!, responseTitle: e.response!.statusMessage, responseBody: e.response!.data, responseDetails: e, extractedMessage: "${e.message}");
      } else if (e is DioException) {
        if (e.type == DioExceptionType.unknown) {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        } else if (e.type == DioExceptionType.connectionTimeout || e.type == DioExceptionType.receiveTimeout || e.type == DioExceptionType.sendTimeout) {
          response = NetworkResponse(
              responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection Timeout ,Can not Reach to Server!", responseDetails: e, extractedMessage: "Connection Timeout ,Can not Reach to Server!");
        } else if (e.type == DioExceptionType.cancel) {
          response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Request Canceled!", responseDetails: e, extractedMessage: "Request Canceled!");
        } else {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        }
      } else {
        response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Failed", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: e.toString());
      }
    }

    response.isCancelable = isCancelable;
    response.callDefaults = callDefaults;
    response.handleSuccess = handleSuccess;
    response.handleErrors = handleErrors;

    response.retryFunction = retry ?? (_,__) => get.call();
    log(response.toString());

    if (options.tokenExpireRule(response, this)) {
      options.onTokenExpire(response, this);
      response.extractedMessage = "Token Expired!";
      response.responseCode = -999;
      response.responseStatus = false;
    }

    if (!options.extraSuccessRule(response, this)) {
      response.responseStatus = false;
      response.responseCode = -100;
      response.responseTitle = "Success Rules Failed";
    }

    if (callDefaults) {
      if (response.responseStatus) {
        options.onSuccessDefault(response, this);
      } else {
        options.onFailedDefault(response, this);
      }
      options.onEndDefault(response, this);
    } else {
      if (response.responseStatus) {
        if (handleSuccess) {
          options.onSuccessDefault(response, this);
        }
      } else {
        if (handleErrors) {
          options.onFailedDefault(response, this);
        }
      }
    }

    return response;
  }

  Future<NetworkResponse> put() async {
    if (callDefaults) {
      options.onStartDefault(this);
    }
    NetworkResponse response;
    try {
      log("Putting ${jsonEncode(data)} in ${jsonEncode(api)}");
      Response res = await dio.put(api, data: data, cancelToken: options.globalCancelToken, options: options.option).catchError((e) {
        throw e;
      }).timeout(timeOut, onTimeout: () {
        throw DioExceptionType.connectionTimeout;
      });

      bool isJson = res.headers.value("content-type")!.contains("json");
      if (!isJson && !useCompression) {
        res.data = jsonDecode(res.data);
      }
      res.data = decompressData(res.data, useCompression, compressDataKey: compressDataKey);
      response = NetworkResponse(
          responseStatus: res.statusCode! > 199 && res.statusCode! < 300,
          responseCode: res.statusCode!,
          responseTitle: res.statusMessage,
          responseBody: res.data,
          responseDetails: res,
          extractedMessage: options.successMsgExtractor!(res.data));
    } catch (e) {
      if (e is DioException && e.response != null && e.response!.data is Map) {
        response = NetworkResponse(
            responseStatus: e.response!.statusCode! > 199 && e.response!.statusCode! < 300,
            responseCode: e.response!.statusCode!,
            responseTitle: e.response!.statusMessage,
            responseBody: e.response!.data,
            responseDetails: e,
            extractedMessage: options.errorMsgExtractor(e.response!.data));
      } else if (e is DioException && e.type == DioExceptionType.badResponse) {
        response = NetworkResponse(responseStatus: false, responseCode: e.response!.statusCode!, responseTitle: e.response!.statusMessage, responseBody: e.response!.data, responseDetails: e, extractedMessage: "${e.message}");
      } else if (e is DioException) {
        if (e.type == DioExceptionType.unknown) {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        } else if (e.type == DioExceptionType.connectionTimeout || e.type == DioExceptionType.receiveTimeout || e.type == DioExceptionType.sendTimeout) {
          response = NetworkResponse(
              responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection Timeout ,Can not Reach to Server!", responseDetails: e, extractedMessage: "Connection Timeout ,Can not Reach to Server!");
        } else if (e.type == DioErrorType.cancel) {
          response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Request Canceled!", responseDetails: e, extractedMessage: "Request Canceled!");
        } else {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        }
      } else {
        response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Failed", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: e.toString());
      }
    }

    response.isCancelable = isCancelable;
    response.callDefaults = callDefaults;
    response.handleSuccess = handleSuccess;
    response.handleErrors = handleErrors;

    response.retryFunction = retry ?? (_,__) => put.call();
    log(response.toString());

    if (options.tokenExpireRule(response, this)) {
      options.onTokenExpire(response, this);
      response.extractedMessage = "Token Expired!";
      response.responseCode = -999;
      response.responseStatus = false;
    }

    if (!options.extraSuccessRule(response, this)) {
      response.responseStatus = false;
      response.responseCode = -100;
      response.responseTitle = "Success Rules Failed";
    }

    if (callDefaults) {
      if (response.responseStatus) {
        options.onSuccessDefault(response, this);
      } else {
        options.onFailedDefault(response, this);
      }
      options.onEndDefault(response, this);
    } else {
      if (response.responseStatus) {
        if (handleSuccess) {
          options.onSuccessDefault(response, this);
        }
      } else {
        if (handleErrors) {
          options.onFailedDefault(response, this);
        }
      }
    }

    return response;
  }

  Future<NetworkResponse> delete() async {
    if (callDefaults) {
      options.onStartDefault(this);
    }
    NetworkResponse response;
    try {
      log("Deleting ${jsonEncode(data)} from ${jsonEncode(api)}");
      Response res = await dio.delete(api, data: data, cancelToken: options.globalCancelToken, options: options.option).catchError((e) {
        throw e;
      }).timeout(timeOut, onTimeout: () {
        throw DioExceptionType.connectionTimeout;
      });

      bool isJson = res.headers.value("content-type")!.contains("json");
      if (!isJson && !useCompression) {
        res.data = jsonDecode(res.data);
      }
      res.data = decompressData(res.data, useCompression, compressDataKey: compressDataKey);
      response = NetworkResponse(
          responseStatus: res.statusCode! > 199 && res.statusCode! < 300,
          responseCode: res.statusCode!,
          responseTitle: res.statusMessage,
          responseBody: res.data,
          responseDetails: res,
          extractedMessage: options.successMsgExtractor!(res.data));
    } catch (e) {
      if (e is DioException && e.response != null && e.response!.data is Map) {
        response = NetworkResponse(
            responseStatus: e.response!.statusCode! > 199 && e.response!.statusCode! < 300,
            responseCode: e.response!.statusCode!,
            responseTitle: e.response!.statusMessage,
            responseBody: e.response!.data,
            responseDetails: e,
            extractedMessage: options.errorMsgExtractor(e.response!.data));
      } else if (e is DioException && e.type == DioExceptionType.badResponse) {
        response = NetworkResponse(responseStatus: false, responseCode: e.response!.statusCode!, responseTitle: e.response!.statusMessage, responseBody: e.response!.data, responseDetails: e, extractedMessage: "${e.message}");
      } else if (e is DioException) {
        if (e.type == DioExceptionType.unknown) {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        } else if (e.type == DioExceptionType.connectionTimeout || e.type == DioExceptionType.receiveTimeout || e.type == DioExceptionType.sendTimeout) {
          response = NetworkResponse(
              responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection Timeout ,Can not Reach to Server!", responseDetails: e, extractedMessage: "Connection Timeout ,Can not Reach to Server!");
        } else if (e.type == DioExceptionType.cancel) {
          response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Request Canceled!", responseDetails: e, extractedMessage: "Request Canceled!");
        } else {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        }
      } else {
        response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Failed", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: e.toString());
      }
    }

    response.isCancelable = isCancelable;
    response.callDefaults = callDefaults;
    response.handleSuccess = handleSuccess;
    response.handleErrors = handleErrors;

    response.retryFunction = retry ?? (_,__) => delete.call();
    log(response.toString());

    if (options.tokenExpireRule(response, this)) {
      options.onTokenExpire(response, this);
      response.extractedMessage = "Token Expired!";
      response.responseCode = -999;
      response.responseStatus = false;
    }

    if (!options.extraSuccessRule(response, this)) {
      response.responseStatus = false;
      response.responseCode = -100;
      response.responseTitle = "Success Rules Failed";
    }

    if (callDefaults) {
      if (response.responseStatus) {
        options.onSuccessDefault(response, this);
      } else {
        options.onFailedDefault(response, this);
      }
      options.onEndDefault(response, this);
    } else {
      if (response.responseStatus) {
        if (handleSuccess) {
          options.onSuccessDefault(response, this);
        }
      } else {
        if (handleErrors) {
          options.onFailedDefault(response, this);
        }
      }
    }

    return response;
  }

  Future<NetworkResponse> download() async {
    if (callDefaults) {
      options.onStartDefault(this);
    }
    NetworkResponse response;
    try {
      log("Downloading ${jsonEncode(data)} from ${jsonEncode(api)}");
      Response res = await dio.download(api, savePath, onReceiveProgress: onReceiveProgress, data: data, cancelToken: options.globalCancelToken).catchError((e) {
        throw e;
      }).timeout(timeOut, onTimeout: () {
        throw DioExceptionType.connectionTimeout;
      });
      response = NetworkResponse(
          responseStatus: res.statusCode! > 199 && res.statusCode! < 300,
          responseCode: res.statusCode!,
          responseTitle: res.statusMessage,
          responseBody: res.data,
          responseDetails: res,
          extractedMessage: options.successMsgExtractor!(res.data));
    } catch (e) {
      if (e is DioException && e.response != null && e.response!.data is Map) {
        response = NetworkResponse(
            responseStatus: e.response!.statusCode! > 199 && e.response!.statusCode! < 300,
            responseCode: e.response!.statusCode!,
            responseTitle: e.response!.statusMessage,
            responseBody: e.response!.data,
            responseDetails: e,
            extractedMessage: options.errorMsgExtractor(e.response!.data));
      } else if (e is DioException && e.type == DioExceptionType.badResponse) {
        response = NetworkResponse(responseStatus: false, responseCode: e.response!.statusCode!, responseTitle: e.response!.statusMessage, responseBody: e.response!.data, responseDetails: e, extractedMessage: "${e.message}");
      } else if (e is DioException) {
        if (e.type == DioExceptionType.unknown) {
          response = NetworkResponse(
              responseStatus: false,
              responseCode: -500,
              responseTitle: "Error",
              responseBody: "Connection interrupted ,Can not Connect to Server!",
              responseDetails: e,
              extractedMessage: "Connection interrupted ,Can not Connect to Server!");
        } else if (e.type == DioExceptionType.connectionTimeout || e.type == DioExceptionType.receiveTimeout || e.type == DioExceptionType.sendTimeout) {
          response = NetworkResponse(
              responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection Timeout ,Can not Reach to Server!", responseDetails: e, extractedMessage: "Connection Timeout ,Can not Reach to Server!");
        } else if (e.type == DioExceptionType.cancel) {
          response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Request Canceled!", responseDetails: e, extractedMessage: "Request Canceled!");
        } else {
          response =
              NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Error", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: "Something went wrong");
        }
      } else {
        response = NetworkResponse(responseStatus: false, responseCode: -500, responseTitle: "Failed", responseBody: "Connection interrupted ,Can not Connect to Server!", responseDetails: e, extractedMessage: e.toString());
      }
    }

    response.isCancelable = isCancelable;
    response.callDefaults = callDefaults;
    response.handleSuccess = handleSuccess;
    response.handleErrors = handleErrors;

    response.retryFunction = retry ?? (_,__) => delete.call();
    log(response.toString());

    if (options.tokenExpireRule(response, this)) {
      options.onTokenExpire(response, this);
      response.extractedMessage = "Token Expired!";
      response.responseCode = -999;
      response.responseStatus = false;
    }

    if (!options.extraSuccessRule(response, this)) {
      response.responseStatus = false;
      response.responseCode = -100;
      response.responseTitle = "Success Rules Failed";
    }

    if (callDefaults) {
      if (response.responseStatus) {
        options.onSuccessDefault(response, this);
      } else {
        options.onFailedDefault(response, this);
      }
      options.onEndDefault(response, this);
    } else {
      if (response.responseStatus) {
        if (handleSuccess) {
          options.onSuccessDefault(response, this);
        }
      } else {
        if (handleErrors) {
          options.onFailedDefault(response, this);
        }
      }
    }

    return response;
  }
}

Map<String, dynamic> decompressData(dynamic data, bool useCompression, {String compressDataKey = "Body"}) {
  if (useCompression) {
    var hexData = data.toString().substring(2);
    // print(hexData);
    final decodedData = hex.decode(hexData);
    // final unzippedData = gzip.decode(decodedData);
    var unzippedData = kIsWeb ? GZipDecoder().decodeBytes(decodedData) : gzip.decode(decodedData);
    unzippedData = unzippedData.where((element) => element != 0).toList();
    // File f = File('tt.json');
    // try {
    //   f.writeAsBytesSync(unzippedData);
    // }catch (e){
    //   print(e);
    // }
    // print(unzippedData);
    final decoded = utf8.decode(unzippedData, allowMalformed: true);
    // print(decoded);
    var decodedFix = decoded.characters.where((p0) => p0 != "\u0000").join();
    // var decodedFix = decoded;
    // decodedFix = decodedFix.replaceAll('', "");
    // print(decodedFix.runtimeType);
    var mapData = jsonDecode(decodedFix);
    // var mapData = decodedFix;
    // print(mapData);
    return mapData;
  } else {
    return data;
  }
}
