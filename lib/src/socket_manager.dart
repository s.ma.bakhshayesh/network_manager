import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:network_manager/src/uis/MyDialog.dart';
import 'package:network_manager/src/uis/MyFancyDialog.dart';

class SocketManagerMetaData {
  String? tcpAddress;
  int? tcpPort;

  ///called on start every request
  Function onStartDefault = () => log("start default");

  ///called on End every request
  Function onEndDefault = () => log("end default");

  ///called on Success every request
  Function onSuccessDefault = () => log("success default");

  ///called on Failed every request
  Function onFailedDefault = () => log("failed default");

  Future Function(BuildContext context, String title, String body)?
      successDialog;
  Future Function(BuildContext context, String title, String body)? failedDialog;

  Duration? timeOut;

  bool Function(dynamic responseJson) successRules =
      (dynamic responseJson) => false;
  bool Function(dynamic responseJson) failedRules =
      (dynamic responseJson) => false;
  bool Function(dynamic responseJson) endRules =
      (dynamic responseJson) => false;

  ///conditions where token expires
  bool Function(dynamic responseJson) tokenExpireRules =
      (dynamic responseJson) => false;

  ///called on token expire
  void Function(void Function() retryFunctoin) onTokenExpire = (_) {};

  /// a function to extract error as string
  String Function(dynamic error) errorMsgExtractor =
      (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  bool useFancyDialog = true;

  static final SocketManagerMetaData _singleton =
      SocketManagerMetaData._internal();

  SocketManagerMetaData._internal();

  factory SocketManagerMetaData() {
    return _singleton;
  }

  static initialize({
    String? tcpAddress,
    int? tcpPort,

    ///millisecond
    Duration timeout = const Duration(milliseconds: 15000),
    Function? onStartDefault,
    Function? onEndDefault,
    Function? onSuccessDefault,
    Function? onFailedDefault,
    bool useFancyDialog = false,
    bool Function(dynamic responseJson)? successRules,
    bool Function(dynamic responseJson)? tokenExpireRules,
    Future Function(BuildContext context, String title, String body)?
        successDialog,
    Future Function(BuildContext context, String title, String body)?
        failedDialog,
    void Function(void Function() retryFunctoin)? onTokenExpire,
    String Function(dynamic error)? errorMsgExtractor,
    String Function(dynamic msg)? successMsgExtractor,
  }) {
    log("Initializing Socket Manager");
    if (onStartDefault != null) {
      _singleton.onStartDefault = onStartDefault;
    }
    if (onEndDefault != null) {
      _singleton.onEndDefault = onEndDefault;
    }
    if (onSuccessDefault != null) {
      _singleton.onSuccessDefault = onSuccessDefault;
    }
    if (onFailedDefault != null) {
      _singleton.onFailedDefault = onFailedDefault;
    }
    if (successRules != null) {
      _singleton.successRules = successRules;
    }
    if (tokenExpireRules != null) {
      _singleton.tokenExpireRules = tokenExpireRules;
    }
    if (onTokenExpire != null) {
      _singleton.onTokenExpire = onTokenExpire;
    }
    if (tcpAddress != null) {
      _singleton.tcpAddress = tcpAddress;
    }
    if (tcpPort != null) {
      _singleton.tcpPort = tcpPort;
    }
      _singleton.timeOut = timeout;
    if (successDialog != null) {
      _singleton.successDialog = successDialog;
    }
    if (failedDialog != null) {
      _singleton.failedDialog = failedDialog;
    }
      _singleton.useFancyDialog = useFancyDialog;
    if (errorMsgExtractor != null) {
      _singleton.errorMsgExtractor = errorMsgExtractor;
    }
    if (successMsgExtractor != null) {
      _singleton.successMsgExtractor = errorMsgExtractor;
    }
  }
}

class SocketManager {
  BuildContext? context;
  String? tcpAddress;
  Duration? timeOutSeconds;
  int? port;
  Socket? _socket;

  ///other rules for check beside result code 200 should be a function return bool value
  bool Function(dynamic responseJson)? successRules =
      (dynamic responseJson) => true;
  bool Function(dynamic responseJson)? failedRules =
      (dynamic responseJson) => false;
  bool Function(dynamic responseJson)? endRules =
      (dynamic responseJson) => false;

  ///conditions where token expires
  bool Function(dynamic responseJson)? tokenExpireRules =
      (dynamic responseJson) => false;

  ///called on token expire
  void Function(void Function() retryFunctoin)? onTokenExpire = (_) {};

  /// a function to extract error as string
  String Function(dynamic error)? errorMsgExtractor =
      (error) => error.toString();
  String Function(dynamic msg)? successMsgExtractor = (msg) => msg.toString();

  ///called on start this instance of Network Manager request
  Function onStart = () {};

  ///called on end this instance of Network Manager request
  Function onEnd = () {};

  void Function(String data)? onDataReceived = (data) {};

  ///called on success this instance of Network Manager request
  void Function(BuildContext context, SMResponse responseJson) onSuccess =
      (BuildContext context, SMResponse responseJson) {};

  ///called on failed this instance of Network Manager request
  void Function(BuildContext context, SMResponse responseJson) onFailed =
      (BuildContext context, SMResponse responseJson) {};

  ///check to show dialog when when request is success
  bool? showFailedDialog = false;

  ///check to show dialog when when request is failed
  bool? showSuccessDialog = false;

  ///check to call default functions each step
  bool? callDefaults = true;
  bool? callCheckers = true;

  bool? useFancyDialog = true;

  ///called on Success every request
  Function? onSuccessDefault = () {};

  ///called on Failed every request
  Function? onFailedDefault = () {};

  Function? onStartDefault = () {};

  ///called on End every request
  Function? onEndDefault = () {};

  Future Function(BuildContext context, String title, String body)?
      successDialog;
  Future Function(BuildContext context, String title, String body)? failedDialog;

  SocketManager._(
      {required this.context,
      required this.tcpAddress,
      required this.port,
      required this.timeOutSeconds,
      required this.onStart,
      required this.onEnd,
      required this.onFailed,
      required this.onSuccess,
      this.errorMsgExtractor,
      this.useFancyDialog,
      this.callDefaults,
      this.onTokenExpire,
      this.showFailedDialog,
      this.showSuccessDialog,
      this.successMsgExtractor,
      this.successRules,
      this.tokenExpireRules,
      this.onFailedDefault,
      this.onSuccessDefault,
      this.successDialog,
      this.onEndDefault,
      this.onStartDefault,
      this.failedRules,
      this.endRules,
      this.callCheckers,
      this.onDataReceived,
      this.failedDialog});

  factory SocketManager.instance({
    required BuildContext context,
    void Function(BuildContext context, SMResponse result)? onSuccess,
    void Function(BuildContext context, SMResponse result)? onFailed,
    required void Function(String data) onDataReceived,
    Function? onStart,
    Function? onEnd,
    Duration? timeout,
    bool showFailedDialog = true,
    bool callDefaults = true,
    bool callCheckers = true,
  }) {
    SocketManagerMetaData meta = SocketManagerMetaData();

    return SocketManager._(
      context: context,
      tcpAddress: meta.tcpAddress,
      onSuccess:
          onSuccess ?? (BuildContext context, SMResponse responseJson) {},
      onFailed: onFailed ?? (BuildContext context, SMResponse responseJson) {},
      onStart: onStart ?? () {},
      onEnd: onEnd ?? () {},
      showFailedDialog: showFailedDialog,
      callDefaults: callDefaults,
      onSuccessDefault: meta.onSuccessDefault,
      onFailedDefault: meta.onFailedDefault,
      onStartDefault: meta.onStartDefault,
      onEndDefault: meta.onEndDefault,
      successRules: meta.successRules,
      failedRules: meta.failedRules,
      endRules: meta.endRules,
      callCheckers: callCheckers,
      tokenExpireRules: meta.tokenExpireRules,
      onTokenExpire: meta.onTokenExpire,
      successMsgExtractor: meta.successMsgExtractor,
      errorMsgExtractor: meta.errorMsgExtractor,
      successDialog: meta.successDialog,
      useFancyDialog: meta.useFancyDialog,
      failedDialog: meta.failedDialog,
      timeOutSeconds: meta.timeOut,
      onDataReceived: onDataReceived,
      port: meta.tcpPort,
    );
  }

  Future<Socket?> _connectSocket() async {
    if (callDefaults!) {
      onStartDefault!();
    }
    onStart();
    log("Connecting Socket $tcpAddress :$port");
    await Socket.connect(tcpAddress, port!).then((value) {
      if (callDefaults!) {
        onEndDefault!();
        onSuccessDefault!();
      }
      onEnd();
      onSuccess(
          context!,
          SMResponse(
              isSucceed: true,
              responseTitle: "Socked Connected",
              responseBody:
                  "Socket Connected Successfully $tcpAddress :$port"));
      log("Socket Connected Successfully $tcpAddress :$port");
      _socket = value;

      _socket!.handleError((e) {
        if (callDefaults!) {
          onEndDefault!();
          onFailedDefault!();
        }
        onEnd();
        onFailed(
          context!,
          SMResponse(
            data: e,
            isSucceed: false,
            responseCode: -1,
            responseBody: "Error listening to TCP:  $e",
            responseTitle: "Failed Operation",
          ),
        );
        if (showFailedDialog! && context != null) {
          if (failedDialog == null) {
            showDialog(
                context: context!,
                builder: (context) => useFancyDialog!
                    ? MyFancyDialog(
                        title: "Failed Operation",
                        body: "Error listening to TCP:  $e",
                      )
                    : MyDialog(
                        title: "Failed Operation",
                        body: "Error listening to TCP:  $e",
                        type: DialogType.warning,
                      )).then((value) {
              print("dialog returned: " + value.toString());
            });
          } else {
            failedDialog!(
                    context!, "Failed Operation", "Error listening to TCP:  $e")
                .then((value) {
              print("dialog returned: " + value.toString());
            });
          }
        }
      });
      String fullReceivedString = "";
      _socket!.listen(
          (List<int> event) {
            log("Listened event is: [" + utf8.decode(event).toString() + "]");
            fullReceivedString =
                fullReceivedString + utf8.decode(event).toString();
            fullReceivedString = utf8.decode(event).toString();
            onDataReceived!(fullReceivedString);
            if (callCheckers!) {
              if (tokenExpireRules!(fullReceivedString)) {
                onTokenExpire!(() => print("tokenExpire"));
              } else if (successRules!(fullReceivedString)) {
                onSuccess(
                    context!,
                    SMResponse(
                        isSucceed: true,
                        responseCode: 1,
                        data: fullReceivedString));
                onSuccessDefault!();
              } else if (failedRules!(fullReceivedString)) {
                onFailed(
                    context!,
                    SMResponse(
                        isSucceed: false,
                        responseCode: -1,
                        data: fullReceivedString));
                onFailedDefault!();
              } else if (endRules!(fullReceivedString)) {
                if (callDefaults!) {
                  onEndDefault!();
                  onSuccessDefault!();
                }
                onEnd();
              }
            }
          },
          onDone: () {
            log("Error listening to TCP:  Done!");
//        _socket = null;
//        if (callDefaults) {
//          onFailedDefault();
//          onEndDefault();
//        }
//        onFailed(context!, SMResponse(data: "Done", isSucceed: false, responseCode: -1, responseTitle: "Listening Error"));
//        if (showFailedDialog && context != null) {
//          if (failedDialog == null) {
//            showDialog(
//                context: context!,
//                builder: (context) => useFancyDialog
//                    ? MyFancyDialog(
//                  title: "Failed Operation",
//                  body: "Error listening to TCP:   Done!",
//                ) : MyDialog(
//                  title: "Failed Operation",
//                  body: "Error listening to TCP:   Done!",
//                  type: DialogType.warning,
//                )).then((value) {
//              print("dialog returned: " + value.toString());
//            });
//          } else {
//            failedDialog(context!, "Failed Operation", "Error listening to TCP:   Done!").then((value) {
//              print("dialog returned: " + value.toString());
//            });
//          }
//        }
//        onEnd();
          },
          cancelOnError: false,
          onError: (e) {
            String errStr = e.toString();
            String errTitle = "Listening Error";
            if (e.toString().contains("SocketException")) {
              errStr = "No Internet";
              errTitle = "Network Unreachable";
            }
            log("Error listening to TCP:  $e");
            _socket = null;
            if (callDefaults!) {
              onFailedDefault!();
              onEndDefault!();
            }
            onFailed(
                context!,
                SMResponse(
                    data: e,
                    isSucceed: false,
                    responseCode: -1,
                    responseTitle: errTitle,
                    responseBody: errStr));
            if (showFailedDialog! && context != null) {
              if (failedDialog == null) {
                showDialog(
                    context: context!,
                    builder: (context) => useFancyDialog!
                        ? MyFancyDialog(
                            title: "Failed Operation",
                            body: "Error listening to TCP:  $e",
                          )
                        : MyDialog(
                            title: "Failed Operation",
                            body: "Error listening to TCP:  $e",
                            type: DialogType.warning,
                          )).then((value) {
                  print("dialog returned: " + value.toString());
                });
              } else {
                failedDialog!(context!, "Failed Operation",
                        "Error listening to TCP:  $e")
                    .then((value) {
                  print("dialog returned: " + value.toString());
                });
              }
            }
            onEnd();
            throw ("$errTitle: $errStr");
//            return null;
          });
      return _socket;
    }, onError: (e) {
      String errStr = e.toString();
      String errTitle = "Listening Error";
      if (e.toString().contains("SocketException")) {
        errStr = "No Internet";
        errTitle = "Network Unreachable";
      }
      log("Error listening to TCP:  $e");
      _socket = null;

      if (callDefaults!) {
        onFailedDefault!();
        onEndDefault!();
      }
      onFailed(
          context!,
          SMResponse(
              data: e,
              isSucceed: false,
              responseCode: -1,
              responseTitle: errTitle,
              responseBody: errStr));
      if (showFailedDialog! && context != null) {
        if (failedDialog == null) {
          showDialog(
              context: context!,
              builder: (context) => useFancyDialog!
                  ? MyFancyDialog(
                      title: errTitle,
                      body: "Error listening to TCP:  $errStr",
                    )
                  : MyDialog(
                      title: errTitle,
                      body: "Error listening to TCP:  $errStr",
                      type: DialogType.warning,
                    )).then((value) {
            print("dialog returned: " + value.toString());
          });
        } else {
          failedDialog!(context!, errTitle, "Error listening to TCP:  $errStr")
              .then((value) {
            print("dialog returned: " + value.toString());
          });
        }
      }
      onEnd();
      throw ("$errTitle: $errStr");
    }).catchError((e) {
      log("Error Connecting Socket: $e");
      if (e.toString().indexOf("SocketException") > -1) {
        onFailed(
            context!,
            SMResponse(
                data: e,
                isSucceed: false,
                responseCode: -1,
                responseTitle: "Socket Exception",
                responseBody: e.toString()));
        if (showFailedDialog! && context != null) {
          if (failedDialog == null) {
            showDialog(
                context: context!,
                builder: (context) => useFancyDialog!
                    ? MyFancyDialog(
                        title: "Socket Exception",
                        body: "$e",
                      )
                    : MyDialog(
                        title: "Socket Exception",
                        body: "$e",
                        type: DialogType.warning,
                      )).then((value) {
              print("dialog returned: " + value.toString());
            });
          } else {
            failedDialog!(context!, "Socket Exception", " $e").then((value) {
              print("dialog returned: " + value.toString());
            });
          }
        }
        onEnd();
      } else {
        onFailed(
            context!,
            SMResponse(
                data: e,
                isSucceed: false,
                responseCode: -5,
                responseTitle: "Error Connecting Socket",
                responseBody: e));
        if (showFailedDialog! && context != null) {
          if (failedDialog == null) {
            showDialog(
                context: context!,
                builder: (context) => useFancyDialog!
                    ? MyFancyDialog(
                        title: "Error Connecting Socket",
                        body: "$e",
                      )
                    : MyDialog(
                        title: "Error Connecting Socket",
                        body: "$e",
                        type: DialogType.warning,
                      )).then((value) {
              print("dialog returned: " + value.toString());
            });
          } else {
            failedDialog!(context!, "Error Connecting Socket", " $e")
                .then((value) {
              print("dialog returned: " + value.toString());
            });
          }
        }
        onEnd();
      }
      throw ("Error Connecting Socket: $e");
    }).timeout(timeOutSeconds!, onTimeout: () {
      log("Timeout Happened ${timeOutSeconds!.inMilliseconds} milliseconds");
      onFailed(
        context!,
        SMResponse(
          data: "Socket Connection Timeout",
          isSucceed: false,
          responseCode: -2,
          responseTitle: "Timeout",
        ),
      );
      if (callDefaults!) {
        onFailedDefault!();
        onEndDefault!();
      }
      if (showFailedDialog! && context != null) {
        if (failedDialog == null) {
          showDialog(
              context: context!,
              builder: (context) => useFancyDialog!
                  ? MyFancyDialog(
                      title: "Timeout",
                      body: "Socket Connection Timeout",
                    )
                  : MyDialog(
                      title: "Timeout",
                      body: "Socket Connection Timeout",
                      type: DialogType.warning,
                    )).then((value) {
            print("dialog returned: " + value.toString());
          });
        } else {
          failedDialog!(context!, "Timeout", "Socket Connection Timeout")
              .then((value) {
            print("dialog returned: " + value.toString());
          });
        }
      }
      _socket = null;
      throw ("Timeout Happened ${timeOutSeconds!.inMilliseconds} milliseconds");
    } );
    return _socket;
  }

  _sendDataToSocket(String sendData,
      {Duration? waitForResponse = const Duration(milliseconds: 2000)}) async {
    onStart();
    if (callDefaults!) {
      onStartDefault!();
    }
    if (_socket != null) {
      _socket!.add(utf8.encode(sendData));
      log("Sent Data is: " +
          (sendData.length < 200 ? sendData : sendData.substring(0, 199)));
      await Future.delayed(waitForResponse!);
      if (_socket != null) {
        log("Socket is force destroyed after wait time ${waitForResponse.inMilliseconds} milliseconds");
        _socket!.destroy();
        _socket = null;
        onFailed(
          context!,
          SMResponse(
              data: "Socket Connection Timeout",
              isSucceed: false,
              retryFunction: () =>
                  _sendDataToSocket(sendData, waitForResponse: waitForResponse),
              responseCode: -2,
              responseTitle: "Connection Timeout",
              responseBody: "Poor Network: Timeout threshold reached"),
        );
        if (callDefaults!) {
          onFailedDefault!();
          onEndDefault!();
        }
        if (showFailedDialog! && context != null) {
          if (failedDialog == null) {
            showDialog(
                context: context!,
                builder: (context) => useFancyDialog!
                    ? MyFancyDialog(
                        title: "Connection Timeout",
                        body: "Poor Network: Timeout threshold reached",
                      )
                    : MyDialog(
                        title: "Connection Timeout",
                        body: "Poor Network: Timeout threshold reached",
                        type: DialogType.warning,
                      )).then((value) {
              print("dialog returned: " + value.toString());
            });
          } else {
            failedDialog!(context!, "Connection Timeout",
                    "Poor Network: Timeout threshold reached")
                .then((value) {
              print("dialog returned: " + value.toString());
            });
          }
        }
      }
    } else {
      log('Socket is not Connected');
      if (callDefaults!) {
        onFailedDefault!();
        onEndDefault!();
      }
      onEnd();
      if (showFailedDialog! && context != null) {
        if (failedDialog == null) {
          showDialog(
              context: context!,
              builder: (context) => useFancyDialog!
                  ? MyFancyDialog(
                      title: "Network Unreachable",
                      body: "Socket is not Connected",
                    )
                  : MyDialog(
                      title: "Network Unreachable",
                      body: "Socket is not Connected",
                      type: DialogType.warning,
                    )).then((value) {
            print("dialog returned: " + value.toString());
          });
        } else {
          failedDialog!(
                  context!, "Network Unreachable", "Socket is not Connected")
              .then((value) {
            print("dialog returned: " + value.toString());
          });
        }
      }
      onFailed(
          context!,
          SMResponse(
              isSucceed: false,
              responseTitle: "Network Unreachable",
              responseCode: -3,
              retryFunction: () =>
                  _sendDataToSocket(sendData, waitForResponse: waitForResponse),
              data: "Socket is Not Connected"));
    }
  }

  _disconnectSocket() {
    if (_socket != null) {
      _socket!.destroy();
      _socket = null;
    }
    onEnd();
    if (callDefaults!) {
      onEndDefault!();
    }
    log("Connection Closed");
  }

  Future<Socket?> Function() get connectSocket => _connectSocket;

  Function(String data, {Duration? waitForResponse}) get sendDataToSocket =>
      _sendDataToSocket;

  Function get disconnectSocket => _disconnectSocket;
}

class SMResponse {
  bool? isSucceed;
  int? responseCode;
  String? responseTitle;
  String? responseBody;
  dynamic data;
  Function? retryFunction = () {};

  SMResponse(
      {this.data,
      this.isSucceed,
      this.responseBody,
      this.responseCode,
      this.responseTitle,
      this.retryFunction});

  @override
  String toString() {
    return "($responseCode) $responseTitle \n $responseBody";
  }
}
